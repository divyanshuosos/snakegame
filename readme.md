SNAKE GAME IMPLEMENTATION IN MULTIPLE-USER ENVIRONMENT

This game is totally written in C.

The major objectives of this project are:

Tocreate a snake game that will have all the functionality of traditional snake games.
To introduce multilayer functionality in the game that will allow several players to play a game simultaneously. It should be able to give the experience of a real time multiplayer game to the players.
To make use of keyboard keys to move the snake such as up, down or w, s.
To create simple Single player snake game.
To create Multi-player snake game.
To maintain high score table.
Different modes used in the game •	Easy mode: Here the box size in which the snake is allowed to move around freely is larger in size as compared to the other two modes. Hence, the user finds it a lot easier to control the snake. Chances for the snake to die are quite less •	Medium mode: Here the box size in which the snake is allowed to move around freely is larger in size as compared to the hard mode and smaller than in easy mode. Chances for the snake to die are moderate • Hard mode: Here the box size in which the snake is allowed to move around freely is smallest. Chances for the snake to die are highest

Different Gaming Arena •	Unbounded mode: Here, during the process of travelling, if the snake hits the boundary wall, it does not die, instead it comes out from the opposite wall as though there is a continuation in the movement. •	Bounded mode: Here, during the process of travelling, if the snake hits the boundary wall, the snake dies, and this is counted as a loss of life. This is a tougher mode as compared to the unbounded mode

INSTRUCTIONS:

1)I recommend you to first go through the synopsis so that you can understand what we are going to implement in this game. 2)First see some tutorial on pre-defined function in detail rand()- random function kbhit()- keyboard hit function

3)I have explained each and every line of the code using comments. 4)I have used methods for each functionality. 5)This code is entirely different from other codes and you may find it complex to understand .